use std::fs::File;
use std::path::*;

mod dat_file;
mod bin_extract;
mod block_list;
mod btree;

extern crate clap;

use clap::*;

#[derive(Debug)]
enum Error {
	Str(&'static str),
	IoError(std::io::Error),
	DatFile(dat_file::Error)
}

impl From<std::io::Error> for Error {
	fn from(err: std::io::Error) -> Error {
		Error::IoError(err)
	}
}

impl From<dat_file::Error> for Error {
	fn from(err: dat_file::Error) -> Error {
		Error::DatFile(err)
	}
}


type Result<T> = std::result::Result<T, Error>;


fn get_app<'a, 'b>() ->  App<'a, 'b> {
     let app = clap_app!(datool =>
      
        (version: "0.1")
        (author: "Jason Spashet <jason@spashett.com>")
        (about: "datool - do things with dat files.")
        (@arg debug: -d ... "Sets the level of debugging information")
        (@subcommand list =>
            (about: "list all resource ids")
            (@arg verbose: -v --verbose "Verbose")
        )
    ).setting(AppSettings::ArgRequiredElseHelp);
    app
}


fn main() -> Result<()> {
    let app = get_app();
    let matches = app.get_matches();

    if let Some(_list) = matches.subcommand_matches("list") {
        let manifest_dir = env!("CARGO_MANIFEST_DIR");
        let portal_path = Path::new(manifest_dir).join("test_data").join("client_portal.dat");
        let portal_str = portal_path.to_str().ok_or(Error::Str("can't"))?;

                let mut portal_file = File::open(portal_str)?;
                let mut portal_dat = dat_file::DatFile::new(&mut portal_file);
                portal_dat.open()?;
                portal_dat.print_ids()?;
    }
    Ok(())
}
