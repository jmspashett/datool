use std::io::prelude::*;
use std::io::*;
use std;

use bin_extract::*;

#[derive(Debug)]
enum Error
{
	Str(&'static str),
	IoError(std::io::Error),
}

impl From<std::io::Error> for Error {
	fn from(err: std::io::Error) -> Error {
		Error::IoError(err)
	}
}

impl From<Error> for std::io::Error {
	fn from(err: Error) -> std::io::Error {
		match err {
			Error::IoError(x) => x,
			Error::Str(x) =>  std::io::Error::new(ErrorKind::Other, x)
		}
		
	}
}


type Result<T> = std::result::Result<T, Error>;


pub struct BlockList<'a, RS: 'a> where RS: Read + Seek {
	file: &'a mut RS,
	block_size: usize,
	block_remain: usize,
	next_block: Option<usize>,
}


impl<'a, RS> BlockList<'a, RS> where RS: Read + Seek {

	pub fn new(file: &'a mut RS, block_size: usize) -> Self {
		Self { 
			file, 
			block_size,
			block_remain: 0,
			next_block: Some(0),
		}
	}

	fn read_next(&mut self) -> Result<bool> {
		match self.next_block {
			None => Ok(false),
			Some(pos) => {
				if pos != 0 {
					self.file.seek(SeekFrom::Start(pos as u64))?;
				}
				if let Ok(next) = extract_u32(&mut self.file) {
					self.block_remain = self.block_size - 4;
					self.next_block = match next {
						0 => None,
						_ => Some(next as usize)
					}
				}
				Ok(true)
			}
		}
	}

}


impl<'a, RS> Read for BlockList<'a, RS> where RS: Read + Seek {

	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
		let wanted = buf.len();
		if self.block_remain == 0 {
			self.read_next()?;
		}
		let count = 
			if wanted  > self.block_remain	{ 
				self.block_remain 
			} else { 
				wanted
			};
		if count == 0 { 
			return Ok(0); 
		}
		let mut taken = self.file.by_ref().take(count as u64);
		let n_read = taken.read(buf)?;
		self.block_remain -= n_read;
		Ok(n_read)
	}

}


#[cfg(test)]
mod block_list_tests {
	
	use std::io::*;

	#[test]
	fn read_blocks() {
		let test_data: Vec<u8> = vec![
			// Some junk
			01, 02, 03, 04,

			// blk 1 @ 4
			32, 0, 0, 0, 
			1, 2, 3, 4, 5,	6, 7, 8, 9, 10,

			// blk2 @ 18
			0, 0, 0, 0, // no more blocks
			21, 22, 23, 24, 25, 26, 27, 28, 29, 30,

			// blk3 @ 32
			18, 0, 0, 0, 
			11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
			
		];
		let mut f =  Cursor::new(&test_data);
		f.seek(SeekFrom::Start(4 as u64)).unwrap();

		let mut x = super::BlockList::new(&mut f, 14);
		
		println!();
		let mut result = Vec::new();
		loop {
			let mut buf = [0; 5];
			let r = x.read_exact(&mut buf);
			if let Err(r) = r { 
				println!("An error occurred {:?}", r);
				break;
			}

			
			for x in buf.iter() {
				print!(" {:03}", x);
				result.push(*x);
			}
			println!()
		}
		let expected = vec![
			1, 2, 3, 4, 5,	6, 7, 8, 9, 10,
			11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
			21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
		];
		assert_eq!(result, expected);
	}

}

