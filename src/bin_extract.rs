
use std::io::prelude::*;
use std::boxed::Box;

type Result<T> = ::std::result::Result<T, Box<::std::error::Error>>;

pub fn u32_from_array_le(s: &[u8]) -> u32 {
	let mut result = 0;
	for x in (0..s.len()).rev() {
		result = result << 8;
		result |= s[x] as u32;
	}
	result
}

pub fn extract_u32(c: &mut Read) -> Result<u32> {
	let mut buf32: [u8; 4] = Default::default();
	c.read_exact(&mut buf32)?;
	Ok(u32_from_array_le(&buf32))
}


pub trait BinarySerialise<T> 
{
	fn load(reader: &mut Read) -> Result<T>;
}


impl BinarySerialise<u32> for u32
{
	fn load(reader: &mut Read) -> Result<Self>	{
		extract_u32(reader)
	}
}


pub fn extract_array<T>(s: &mut Read, count: usize) 
	-> Result<::std::vec::Vec<T>>
	where T: BinarySerialise<T>
{
	let mut v = Vec::new();
	for _ in 0..count {
		let x = T::load(s)?;
		v.push(x);
	}
	Ok(v)
}

#[cfg(test)]
mod tests {
	use std::io::*;
	use super::*;

	#[test]
	fn extract_array_u32()  {
		let a: [u8; 13] = [
			1, 0, 0, 0,
			2, 0, 0, 0,
			3, 0, 0, 0,
			99
		];

		let result;

		let mut f =  Cursor::new(&a);
		result = extract_array::<u32>(&mut f, 3).unwrap();
		println!("result = {:?}", result);

		let expected = [ 1, 2, 3 ];
		assert_eq!(&result[..], expected);

		let mut buf: [u8; 1] = [0; 1];
		f.read(&mut buf).unwrap();
		assert_eq!(buf[0], 99);


	}

	#[test]
	fn extract_array_test() {
		
		#[derive(PartialEq, Debug)]
		struct Fish {
			a: u8,
			b: u16,
		}


		impl BinarySerialise<Fish> for Fish
		{
			fn load(reader: &mut Read) -> super::Result<Self>	{
				let mut buf: [u8; 2] = [0, 0];
				reader.read_exact(&mut buf[0..1])?;
				let a = buf[0];
				reader.read_exact(&mut buf)?;
				let b = buf[0] as u16  | (buf[1] as u16) << 8;
				Ok(	Fish { a, b } )
			}
		}



		let input = [
			1, 1, 0,
			2, 2, 0
		];

		let expected: Vec<Fish> = vec![
			Fish { a: 1, b: 1 },
			Fish { a: 2, b: 2 }
		];

		let mut c = Cursor::new(&input);
		let result = extract_array::<Fish>(&mut c, 2);
		assert_eq!(result.unwrap(), expected);
	}

}