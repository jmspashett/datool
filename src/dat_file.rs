
use std::io::prelude::*;
use std::io::*;
use std;


use bin_extract::*;
use block_list::*;
use btree::*;

#[derive(Debug)]
pub enum Error {
	Str(&'static str),
	String(String),
	IoError(std::io::Error),
	FromUtf8Error(std::string::FromUtf8Error)
}

type Result<T> = std::result::Result<T, Error>;
type EmptyResult = Result<()>;

impl From<std::io::Error> for Error {
	fn from(err: std::io::Error) -> Error {
		Error::IoError(err)
	}
}

impl From<std::string::FromUtf8Error> for Error {
	fn from(err: std::string::FromUtf8Error) -> Error {
		Error::FromUtf8Error(err)
	}
}

impl From<Box<std::error::Error>> for Error {
	fn from(err: Box<std::error::Error>) -> Error {
		Error::String(err.description().to_owned())
	}
}


/* Starts at 0x12c */
#[allow(dead_code)]
const PRE_TOD_HEADER_OFS: u64 = 0x12c;
#[allow(dead_code)]
struct DatHeaderPreTod
{
	file_type: u32, // 0x00 'TB' !
	block_size: u32, // 0x04 0x400 for PORTAL : 0x100 for CELL
	file_size: u32, // 0x08 Should match file size.
	iteration: u32, // 0x0C Version iteration.
	free_head: u32 , // 0x10
	free_tail: u32, // 0x14
	free_count: u32, // 0x18
	btree_ofs: u32, // 0x1C BTree offset
	unknown: [u32; 3],  // 0x20
}

/* Starts at 0x140*/
const POST_TOD_HEADER_OFS: u64 = 0x140;

#[derive(Debug, PartialEq, Copy, Clone)]
pub enum DatSet 
{
	None,
	Cell, // 2
	Portal // 1
}

impl Default for DatSet {
    fn default() -> Self { DatSet::None }
}


#[derive(Default, Debug)]
struct DatHeaderPostTod
{
	file_type: u32,
	block_size: u32,
	file_size: u32,
	data_set: DatSet, 
	data_subset: u32,
	free_head: u32,
	free_tail: u32,
	free_count: u32,
	btree_ofs: u32,
	new_lru: u32, // 0
	old_lru: u32, // 0
	base_lru: bool, // False
	master_map_id: u32,
	engine_pack_version: u32,
	game_pack_version: u32, // 0
	version_major: [u8; 16],
	version_minor: u32,
}


impl DatHeaderPostTod {
	
	fn from_cursor<T>(h: &mut T) -> Result<DatHeaderPostTod> 
	 	where T: Read + Seek 
	{
		h.seek(SeekFrom::Start(POST_TOD_HEADER_OFS))?;
		let mut head = DatHeaderPostTod {
			file_type: extract_u32(h)?,
			block_size: extract_u32(h)?,
			file_size: extract_u32(h)?,
			data_set: if extract_u32(h)? == 1 { DatSet::Portal } else { DatSet::Cell},
			data_subset: extract_u32(h)?,
			free_head: extract_u32(h)?,
			free_tail: extract_u32(h)?,
			free_count: extract_u32(h)?,
			btree_ofs: extract_u32(h)?,
			new_lru: extract_u32(h)?,
			old_lru: extract_u32(h)?,
			base_lru: extract_u32(h)? == 0,
			master_map_id: extract_u32(h)?,
			engine_pack_version: extract_u32(h)?,
			game_pack_version: extract_u32(h)?,
			version_major: [0; 16], // read after
			version_minor: extract_u32(h)?,
		};
		h.read_exact(&mut head.version_major)?;
		Ok(head)
	}

}

#[allow(dead_code)]
enum DatHeader {
	None,
	PreTod(DatHeaderPreTod),
	PostTod(DatHeaderPostTod)
}

impl DatHeader {

	fn btree_ofs(&self) -> Result<u32> {
		match *self {
			DatHeader::PreTod(ref x) => Ok(x.btree_ofs),
			DatHeader::PostTod(ref x) => Ok(x.btree_ofs),
			DatHeader::None => Err(Error::Str("No header"))
		}
	}

	fn block_size(&self) -> Result<u32> {
		match *self {
			DatHeader::PreTod(ref x) => Ok(x.block_size),
			DatHeader::PostTod(ref x) => Ok(x.block_size),
			DatHeader::None => Err(Error::Str("No header"))
		}
	}

}


pub struct DatFile<'a, RS: 'a + Read + Seek> {
	file: &'a mut RS,
	header: DatHeader
}


impl<'a, RS> DatFile<'a, RS> 
	where RS: 'a + Read + Seek
{
	
	pub fn new(file: & mut RS) -> DatFile<RS> {
		DatFile { file: file, header: DatHeader::None }
	}

	pub fn open(&mut self) -> EmptyResult {
		self.read_header()
	}

	pub fn data_set(&self) -> Option<DatSet> {
		if let &DatHeader::PostTod(ref h) = &self.header {
			Some(h.data_set)
		} else { 
			None
		}
	}

	pub fn print_ids(&mut self) -> Result<()> {
		self.visit_all(|&x| {
			println!("id: 0x{:0x} data {:0x}", x.id, x.block_head);
			Ok(())
		})
	}

	pub fn get_stream_for_id(id: u32) {
		//btree find
	}

	pub fn dump_all() {
		// dump all resources to disk
	}


	fn visit_all(
		&mut self, 
		f: fn(&BtreeDataEntryPostTod) ->  Result<()>)
		-> Result<()> 
	{
		let root_ofs = self.header.btree_ofs()? as u64;
		self.visit_block(root_ofs, f)
	}

	/* Change to iterator */
	fn visit_block(
		&mut self, 
		offset: u64, 
		f: fn(&BtreeDataEntryPostTod) ->  Result<()>
	) 
		-> Result<()> 
	{
		self.file.seek(SeekFrom::Start(offset))?;
		let block_size = self.header.block_size()? as usize;
		let node: BtreeNode;
		{
			let mut block = BlockList::new(self.file, block_size);
			node = BtreeNode::load(&mut block)?;
		}
		println!("Entry count {}", node.entry_count);
		for x in 0..node.entry_count as usize {
			if !node.is_leaf() {
				self.visit_block(node.branches[x] as u64, f)?
			}
			match node.data_entries[x] {
				BtreeDataEntry::PostTod(e) => {
					f(&e)
				},
				_ => Err(Error::Str("Expecting postTod"))
			}?;
		}
		Ok(())
	}



	fn read_header(&mut self) -> EmptyResult {
		let h = DatHeaderPostTod::from_cursor(self.file)?;
		if h.file_type != 0x5442 {
			return Err(Error::Str("Signature 'TB' missing"));
		}
		println!("file_type = 0x{:x}", h.file_type);
		println!("block_size = {}", h.block_size);
		println!("file_size = {:}", h.file_size);
		println!("data_set  = {:?}", h.data_set);
		println!("free_count = 0x{:x}", h.free_count);
		println!("btree_ofs = 0x{:x}", h.btree_ofs);
		println!("version_major = 0x {:?}", h.version_major);

		println!("all = {:?}", h);
		self.header = DatHeader::PostTod(h);
		Ok(())
	}
}


#[cfg(test)]
mod dat_tests {
	
	use std::io::*;

    const CELL_HEADER: &'static [u8] = include_bytes!("test_data/client_cell_1_header_only.dat");
    const PORTAL_HEADER: &'static [u8] = include_bytes!("test_data/client_portal_header_only.dat");    

    #[test]
    fn open_dat() {
    	let mut cell_stream =  Cursor::new(&CELL_HEADER[..]);
  		let mut cell_dat = ::dat_file::DatFile::new(&mut cell_stream);
   		let result = cell_dat.open();
   		if let Err(ref err) = result {
   			println!("Failerure: {:?}", err);
   		}
        assert_eq!(result.is_ok(), true);
    }

    #[test]
    fn cell_file() {
    	let mut cell_stream =  Cursor::new(&CELL_HEADER[..]);
  		let mut dat = ::dat_file::DatFile::new(&mut cell_stream);
   		dat.open().unwrap();
        assert_eq!(dat.data_set().unwrap(), ::dat_file::DatSet::Cell);

    }

    #[test]
    fn portal_file() {
    	let mut cell_stream =  Cursor::new(&PORTAL_HEADER[..]);
  		let mut dat = ::dat_file::DatFile::new(&mut cell_stream);
   		dat.open().unwrap();
        assert_eq!(dat.data_set().unwrap(), ::dat_file::DatSet::Portal);
    }
}


