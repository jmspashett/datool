use std::io::prelude::*;
use bin_extract::*;

type Result<T> = ::std::result::Result<T, Box<::std::error::Error>>;

#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
pub struct BtreeDataEntryPreTod
{
	id: u32,
	block_head: u32,
	length: u32,
}

#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
pub struct BtreeDataEntryPostTod
{
	unknown00: u32,
	pub id: u32,
	pub block_head: u32,
	length: u32,
	timestamp: u32,
	unknown14: u32,
}

impl BinarySerialise<BtreeDataEntryPostTod> for BtreeDataEntryPostTod {
	fn load(reader: &mut Read) -> Result<BtreeDataEntryPostTod> {
		let entry = BtreeDataEntryPostTod {
			unknown00: extract_u32(reader)?,
			id: extract_u32(reader)?,
			block_head: extract_u32(reader)?,
			length: extract_u32(reader)?,
			timestamp: extract_u32(reader)?,
			unknown14: extract_u32(reader)?,
		};
		Ok(entry)
	}
}


#[allow(dead_code)]
#[derive(Debug, Copy, Clone)]
pub enum BtreeDataEntry
{
	PreTod(BtreeDataEntryPreTod),
	PostTod(BtreeDataEntryPostTod)
}

impl BinarySerialise<BtreeDataEntry> for BtreeDataEntry {
	
	fn load(reader: &mut Read) -> Result<BtreeDataEntry> {
		let post_tod = BtreeDataEntryPostTod::load(reader)?;
		Ok(BtreeDataEntry::PostTod(post_tod))
	}

}

pub struct BtreeNode
{
	pub branches: [u32; 0x3e],
	pub entry_count: u32,
	pub data_entries: [BtreeDataEntry; 0x3d]
}

impl BinarySerialise<BtreeNode> for BtreeNode {
	
	fn load(reader: &mut Read) -> Result<BtreeNode>
	{
		let data_entry = BtreeDataEntry::PostTod(BtreeDataEntryPostTod {
			unknown00: 0, 
			id: 0, 
			block_head: 0,
			length: 0, 
			timestamp: 0,
			unknown14: 0
		});

		let mut node = BtreeNode {
			branches: [0; 0x3e],
			entry_count: 0,
			data_entries: [data_entry ; 0x3d]
		};
		let branches = extract_array::<u32>(reader, 0x3e)?;
		node.branches.copy_from_slice(&branches[..]);
		node.entry_count = extract_u32(reader)?;
		let entries = extract_array::<BtreeDataEntry>(reader, 0x3d)?;
		node.data_entries.copy_from_slice(&entries[..]);
		Ok(node)
	}

}

impl BtreeNode {

	fn next_node(&self) -> u32 {
		if self.is_leaf() {
			0
		} else {
			self.branches[self.entry_count as usize]
		}
	}

	pub fn is_leaf(&self) -> bool {
		if self.branches[0] == 0 { true } else { false }
	}

}


#[cfg(test)]
mod tests {
	
	const BTREE_ROOT_POST_DOS: &'static [u8] = 
		include_bytes!("test_data/btree_post_tod_root_node.dat");

	use std::io::*;
	use super::*;
	

	#[test]
	fn read_root() {
		let mut cursor =  Cursor::new(&BTREE_ROOT_POST_DOS[..]);
		let root = BtreeNode::load(&mut cursor).unwrap();
		assert_eq!(root.next_node(), 0x20d74000);
		assert_eq!(root.entry_count, 0x3a);
		assert_eq!(root.branches[root.entry_count as usize], 0x20d74000);
		match root.data_entries[0x20] {
			BtreeDataEntry::PostTod(data_entry) => {
				data_entry_test(&data_entry);
			},
			_ => panic!("Not expected")
		};
	}

	fn data_entry_test(data_entry: &BtreeDataEntryPostTod) {
		assert_eq!(data_entry.unknown00, 0x00020000);
		assert_eq!(data_entry.id, 0x0600452e);
		assert_eq!(data_entry.block_head, 0x073ebc00);
		assert_eq!(data_entry.timestamp, 0x4297a7cf);
		assert_eq!(data_entry.unknown14, 1);
	}

	#[test]
	fn read_post_tod_entry() {
		let an_entry: [u8; 24] =
		[
			0x00, 0x00, 0x02, 0x00, 
			0x8E, 0x0D, 0x00, 0x01, 
			0x00, 0x48, 0x69, 0x02,
			0xF8, 0x00, 0x00, 0x00, 
			0x30, 0xA3, 0x97, 0x42, 
			0x01, 0x00, 0x00, 0x00
		];

		let mut cursor =  Cursor::new(&an_entry[..]);
		let entry = BtreeDataEntry::load(&mut cursor).unwrap();
		match entry {
			BtreeDataEntry::PostTod(data_entry) => {
				assert_eq!(data_entry.id, 0x01000d8e);
			},
			_ => panic!("Not expected")	
		};
	}


}